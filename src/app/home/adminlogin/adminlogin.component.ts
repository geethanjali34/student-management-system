import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../../login.service';
import { Router } from '@angular/router';
import { DataService } from '../../admin/data.service';



@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent {
 
 

  constructor(private router:Router,private hc:HttpClient,private ls:LoginService,private ds:DataService) 
  { }


 submitData(value)
 {
console.log(value);
  if(value.user=="admin")
  {
   
this.ls.adminLogin(value).subscribe(res=>{
  console.log(res);
  if(res["message"]==="Invalid Username")
  {
     alert("Invalid Username");
     this.router.navigate['/adminlogin'];
  }
  if(res["message"]==="Invalid Password")
  {
    
     alert("Invalid Password");
     this.router.navigate['/adminlogin'];
  }
  if(res["message"]==="success")
  {
     alert("Logged in successfully");
     localStorage.setItem("idTokens",res['token']);
     this.router.navigate(['/admin/profilestatus']);
  
  }
   
})

}

else if(value.user=="student")
{
 
this.ls.doLogin(value).subscribe(res=>{
console.log(res);
if(res["message"]==="Invalid Username")
{
   alert("Invalid Username");
   this.router.navigate['/adminlogin'];
}
if(res["message"]==="Invalid Password")
{
   alert("Invalid Password");
   this.router.navigate['/adminlogin'];
}
if(res["message"]==="success")
{
   alert(res['message']);
   localStorage.setItem("idTokens",res['token']);
   console.log(res['data']);
   this.ds.loggedUser(res['data']);
  
   this.router.navigate(['/student/profile']);
  

}
 
})

}

if(value.admin=="")
{
  alert("please enter username and password");
}

/*
  if(value.name=="admin") 
  {
    this.router.navigate(['admin']);

  }
  else                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

  {
    this.router.navigate(['student']);
  }
  */
 }


}
