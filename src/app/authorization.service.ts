import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements HttpInterceptor{
  intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
  {
    //read token from local storage
    
    const idToken=localStorage.getItem("idTokens");
    
    //if token is found,it adds it to header of request object
    if(idToken)
    {
    const cloned=req.clone({
      headers:req.headers.set("Authorization", "Bearer "+idToken)
    });
    //and hten forward the request object cloned with token
    return next.handle(cloned);
    console.log(cloned);
  }
  else{
    //if token is not found,forward the ame req object
    return next.handle(req);
  }
  }


  constructor() { }
}
 
