import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
 //to check login status of user
 isLoggedin:boolean;


  constructor( private hc:HttpClient) { }
  doLogin(userobject):Observable<any>
  {
     return this.hc.post<any>('student/studentlogin',userobject);
  }

 adminLogin(userobject)
 {
  return this.hc.post<any>('admin/adminlogin',userobject);
 }
  
  
}
