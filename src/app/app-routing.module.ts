import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './home/about/about.component';
import { AdminloginComponent } from './home/adminlogin/adminlogin.component';
import { IndexComponent } from './home/index/index.component';
import { ForgotpwdComponent } from './home/forgotpwd/forgotpwd.component';
import { OtpComponent } from './home/otp/otp.component';
import { ChangepwdComponent } from './home/changepwd/changepwd.component';




const routes: Routes = [
  {
    path:'',
    redirectTo:'home',
    pathMatch:'full'
  },
  
  {
    path:'home',
    component:HomeComponent,
     children:[
      {
        path:'',
        redirectTo:'index',
        pathMatch:'full'
      },
      {
        path:'index',
        component:IndexComponent
      },
      {
        path:'about',
        component:AboutComponent
      },
    
      {
        path:'login',
        component:AdminloginComponent
      },
      {
        path:'forgotpwd',
        component:ForgotpwdComponent
      },
      {
        path:'otp',
        component:OtpComponent
      },
      {
        path:'changepwd',
        component:ChangepwdComponent

      }
     ]
  },
  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
