import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fee',
  templateUrl: './fee.component.html',
  styleUrls: ['./fee.component.css']
})
export class FeeComponent implements OnInit {
a:any[];
searchTerm:any;
data:any[];

  constructor(private ds2:DataService,private hc:HttpClient,private router:Router) { }

  ngOnInit() {
    
    this.ds2.sendDataToStudentFee().subscribe(data=>{
      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
      this.data=data['message'];
      }
    })
  }
  submitedData(value)
  {
    if(value.no=="" || value.username=="" || value.name=="" ||value.total=="" || value.paid=="" || value.pending=="")
    {
          alert("Enter valid data")
    }
    else
    {
   this.a=value;
    this.ds2.recieveFromAdminFee(this.a);
    
    this.hc.post('admin/fee',value).subscribe(res=>{
     
      alert(res['message']);
    
    })
  }

}

b:boolean=false;
objectToModify:object;
editingfee(obj)
{
  
  this.objectToModify=obj;
  console.log(this.objectToModify);
    this.b=true;
 
}


onSubmit(value)
{
  this.b=false;
this.hc.put('admin/modifyfee',value).subscribe(res=>{
  if(res['message']=="success")
  {
    alert("Successfully Modified");
  }
})
}


deletingfee(name)
{
  console.log(name);
  this.hc.delete(`/admin/deletingfee/${name}`).subscribe(res=>{
    alert(res['message']);
    this.data=res['remdata']
  });
}

}
