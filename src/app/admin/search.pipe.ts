import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(j: any, searchTerm:string): any {

    if(!searchTerm)
     {
       return j;
     }

    return j.filter(obj=>
      
      obj.username.toLowerCase().indexOf(searchTerm.toLowerCase())!==-1 ||
      obj.name.toLowerCase().indexOf(searchTerm.toLowerCase())!==-1) ;
      
  }

}
