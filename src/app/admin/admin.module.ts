import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { MarksComponent } from './marks/marks.component';
import { NotificationComponent } from './notification/notification.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { FeeComponent } from './fee/fee.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfilestatusComponent } from './profilestatus/profilestatus.component';
import { LeaveComponent } from './leave/leave.component';
import { AddstudentComponent } from './addstudent/addstudent.component';
import { SearchPipe } from './search.pipe';



@NgModule({
  declarations: [AdminComponent, MarksComponent, NotificationComponent, AttendanceComponent, FeeComponent, LogoutComponent, ProfilestatusComponent, LeaveComponent, AddstudentComponent, SearchPipe],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule
    
  ]
})
export class AdminModule { }
