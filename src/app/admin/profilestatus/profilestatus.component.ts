import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profilestatus',
  templateUrl: './profilestatus.component.html',
  styleUrls: ['./profilestatus.component.css']
})
export class ProfilestatusComponent implements OnInit {

  searchTerm:any;

  states:string[]=['Andhra Pradesh','Arunachal Pradesh','Assam','Bihar','Chhattisgarh','Goa','Gujarat','Haryana',
  'Himachal Pradesh','Jammu and Kashmir','Jharkhand','Karnataka','Kerala','Madhya Pradesh','Maharashtra','Manipur','Meghalaya',
  'Mizoram','Nagaland','Odisha','Punjab','Rajasthan','Sikkim','Tamil Nadu','Telangana','Tripura','Uttar Pradesh','Uttarakhand','West Bengal']
  
  genders:string[]=['Female','Male','Others'];
  branches:string[]=['CSE','ECE','EEE','IT','Mechanical','Civil'];
  courses:string[]=['B.Tech','M.Tech','MBA','MCA'];


  i:any[];
  
  j:any[];

  constructor(private ds:DataService,private hc:HttpClient,private router:Router) { }

  ngOnInit() {

    /* return this.ds.sendToAdminProfile(); */

    this.ds.sendToAdminProfile().subscribe(data=>{
      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
    this.j=data['message'];
      }
    })

}

deleteRecord(username)
{
   console.log(username);
   this.hc.delete(`/admin/deletingprofile/${username}`).subscribe(res=>{
     alert(res['message']);
     this.j=res['remdata']
   });
}
b:boolean=false;
objectToModify:object;

editingData(obj)
{
this.objectToModify=obj;
console.log(this.objectToModify);
  this.b=true;
}


onSubmit(value)
{
this.b=false;
this.hc.put('admin/modify',value).subscribe(res=>{
  if(res['message']=="success")
  {
    alert("Successfully Modified");
  }
})
}


}
