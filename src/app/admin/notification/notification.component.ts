import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {


  j:any[]=[];
  constructor(private ds:DataService,private hc:HttpClient) { }

  ngOnInit() {
  }

  subNotifications(value)
  {
    this.j=value;
    this.ds.recieveFromAdminNotify(this.j);
    console.log(this.j);
    this.hc.post('admin/notification',value).subscribe(res=>{
      alert(res['message']);
     
  })

  }
}
