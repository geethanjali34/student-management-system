import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  searchTerm:any;
  branches:string[]=['CSE','ECE','EEE','IT','Mechanical','Civil'];
  courses:string[]=['B.Tech','M.Tech','MBA','MCA'];
  

  constructor(private ds1:DataService,private hc:HttpClient,private router:Router) { }
e:any[];
  ngOnInit() {

    this.ds1.sendToStudentAttend().subscribe(data=>{
      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
      
      this.e=data['message'];
      }
       
    })
  }
  submitData(value)
  {
    if(value.no=="" || value.username=="" || value.name=="" ||value.monthly=="" || value.overall=="")
    {
          alert("Enter valid data")
    }
    else
    {

    
    this.e=value;
    this.ds1.recievedFromAdminAttend(this.e);
   this.hc.post('admin/attendance',value).subscribe(res=>{
    
    alert(res['message']);
   
  })
}

}

b:boolean=false;
objectToModify:object;

editingattendance(obj)
{
  this.objectToModify=obj;
  console.log(this.objectToModify);
    this.b=true;
 
}
onSubmit(value)
{
  this.b=false;
this.hc.put('admin/modifyattendance',value).subscribe(res=>{
  if(res['message']=="success")
  {
    alert("Successfully Modified");
  }
})
}


deletingattendance(name)
{
  console.log(name);
  this.hc.delete(`/admin/deletingattendance/${name}`).subscribe(res=>{
    alert(res['message']);
    this.e=res['remdata']
  });
}

}
