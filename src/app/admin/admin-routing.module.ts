import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { MarksComponent } from './marks/marks.component';


import { NotificationComponent } from './notification/notification.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { FeeComponent } from './fee/fee.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfilestatusComponent } from './profilestatus/profilestatus.component';
import { LeaveComponent } from './leave/leave.component';
import { AddstudentComponent } from './addstudent/addstudent.component';


const routes: Routes = [
  {
    path:'admin',
    component:AdminComponent,
    children:
    [{
      path:'marks',
      component:MarksComponent
    },
    {
      path:'fee',
      component:FeeComponent
    },
    {
      path:'notification',
      component:NotificationComponent
    },
    {
      path:'attendance',
      component:AttendanceComponent
    },
    {
      path:'logout',
      component:LogoutComponent
    },
    {
      path:'profilestatus',
      component:ProfilestatusComponent
    },
    {
      path:'addstudent',
      component:AddstudentComponent
    },
    {
      path:'leave',
      component:LeaveComponent
    }
  ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
