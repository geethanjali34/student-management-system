import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {

  data:any[];
  y:any[];
  searchTerm:any;

  genders:string[]=['Female','Male','Others'];
  branches:string[]=['CSE','ECE','EEE','IT','Mechanical','Civil'];
  courses:string[]=['B.Tech','M.Tech','MBA','MCA'];
  

  constructor(private ds:DataService,private hc:HttpClient,private router:Router) 
  { }

  ngOnInit() {
    this.ds.sendDatatoStudent().subscribe(marks=>{
      if(marks['message']=="unauthorized access")
      {
        alert(marks['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
      this.y=marks['message'];
      }
    })
   
  }

  submitData(value)
  {
  if(value.no=="" || value.username=="" || value.name=="" || value.total=="" || value.gain=="")
    {
          alert("Enter valid data")
    }
    else
  {
    this.data=value;
    console.log(this.data);
    this.ds.recievedDataFromAdmin(this.data);
    this.hc.post('admin/marks',value).subscribe(res=>{
     
      alert(res['message']);
      
  })

}
  }


b:boolean=false;
objectToModify:object;

editingmarks(obj)
{
this.objectToModify=obj;
console.log(this.objectToModify);
  this.b=true;
}


onSubmit(value)
{
this.b=false;
this.hc.put('admin/modifymarks',value).subscribe(res=>{
  if(res['message']=="success")
  {
    alert("Successfully Modified");
  }
})
}

deletingmarks(name)
{
  console.log(name);
  this.hc.delete(`/admin/deletingmarks/${name}`).subscribe(res=>{
    alert(res['message']);
    this.y=res['remdata']
  });
}


}
