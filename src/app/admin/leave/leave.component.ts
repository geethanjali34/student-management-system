import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.css']
})
export class LeaveComponent implements OnInit {

  d:any;
  e:any;
  
  constructor(private ds:DataService,private hc:HttpClient,private router:Router) { }

  ngOnInit() {
   
    this.ds.sendLeave().subscribe(data=>{

      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
      this.d=data['message'];
      }
    })
  }
  acceptRequest(username)
  {
    this.e=this.ds.sendLoggedUser();
    console.log(this.e);
    this.hc.post('/admin/saverequest',({"message":"Request is Accepted","username":username})).subscribe((res)=>{
      alert(res['message'])
     
      this.d=res['data'];

    })
  
  }
  rejectRequest(username)
  {
    this.e=this.ds.sendLoggedUser();
    this.hc.post('/admin/saverequest',({"message":"Request is Rejected","username":username})).subscribe((res)=>{
      alert(res['message'])
      this.d=res['data'];
  })

}
}
