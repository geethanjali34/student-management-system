import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private hc:HttpClient) { }
  x:any[];
  n:object[]=[];
  y:any[];
  z:number;
  h:any[];
  k:any[]=[];
  c:any;
  f:any;
  i:any;
  j:any;
  user;
  recievedDataFromAdmin(data)
  {
    this.x=data;
  }
  sendDatatoStudent():Observable<any[]>
  {
    return this.hc.get<any[]>('student/marksdetails');
  }
  recieveFromAdminFee(a)
  {
      this.n=a;
  }
  sendDataToStudentFee():Observable<any[]>
  {
    return this.hc.get<any[]>('student/feestatus');
  }
  recievedFromAdminAttend(e)
  {
this.y=e;
  }
  sendToStudentAttend():Observable<any[]>
  {
    return this.hc.get<any[]>('student/attendancestatus');
  }
 
  recieveFromStdProfile(g)
  {
this.h=g;
  }
  sendToAdminProfile():Observable<any[]>
  {
   
    return this.hc.get<any[]>('student/profile');
  }

  recieveFromAdminNotify(j)
  {
   this.k=j;
  }
  sendToStudentNotify():Observable<any[]>
  {
    return this.hc.get<any[]>('student/notifications');
  }

  

  recieveLeave(a)
  {
    this.c=a;
  }
  sendLeave():Observable<any[]>
  {
    return this.hc.get<any[]>('admin/leave');
  }

  recieveAccept(e)
  {
    this.f=e;
  }
  sendAccept()
  {
    return this.f;
  }

  recieveReject(h)
  {
    this.i=h;
  }
  sendReject() 
  {
    return this.i;
  }

loggedUser(user)
{
  console.log(user);
  this.user=user[0];
}
sendLoggedUser()
{
  return this.user;
}

specificmarks(user)
{
console.log(user);
return this.hc.post('student/specificstnmarks',user);
}

specificfee(user)
{
  console.log(user);
  return this.hc.post('student/specificstnfee',user);
}

specificprofile(user)
{
  console.log(user);
  return this.hc.post('student/specificstnprofile',user);
}
 

specificattendance(user)
{
  console.log(user);
  return this.hc.post('student/specificstnattendance',user);
}
leaverequest(user)
{
  console.log(user);
  return this.hc.post('student/specificstnrequest',user);
}
}
