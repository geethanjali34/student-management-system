import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {

  states:string[]=['Andhra Pradesh','Arunachal Pradesh','Assam','Bihar','Chhattisgarh','Goa','Gujarat','Haryana',
  'Himachal Pradesh','Jammu and Kashmir','Jharkhand','Karnataka','Kerala','Madhya Pradesh','Maharashtra','Manipur','Meghalaya',
  'Mizoram','Nagaland','Odisha','Punjab','Rajasthan','Sikkim','Tamil Nadu','Telangana','Tripura','Uttar Pradesh','Uttarakhand','West Bengal']
  
  genders:string[]=['Female','Male','Others'];
  branches:string[]=['CSE','ECE','EEE','IT','Mechanical','Civil'];
  courses:string[]=['B.Tech','M.Tech','MBA','MCA'];
  
  data:object[];
    constructor(private ds:DataService,private hc:HttpClient,private router:Router) { }
  
    ngOnInit() {
      this.ds.sendToAdminProfile().subscribe(data=>{
        if(data['message']=="unauthorized access")
        {
          alert(data['message']);
          this.router.navigate(['/home/login']);
        }
        else
        {
      this.data=data['message'];
        }
      })
    }
  
    subData(value)
    {
    
  this.data=value;
  this.ds.recieveFromStdProfile(this.data);
  this.hc.post('admin/addstudent',value).subscribe(res=>{
     alert(res['message']);
  
   
  })

    }

   
  }
  