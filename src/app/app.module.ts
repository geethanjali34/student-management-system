import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AdminModule } from './admin/admin.module';
import { StudentModule } from './student/student.module';
import { AboutComponent } from './home/about/about.component';

import { StudentloginComponent } from './home/studentlogin/studentlogin.component';
import { AdminloginComponent } from './home/adminlogin/adminlogin.component';

import { IndexComponent } from './home/index/index.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AuthorizationService } from './authorization.service';
import { ForgotpwdComponent } from './home/forgotpwd/forgotpwd.component';
import { OtpComponent } from './home/otp/otp.component';
import { ChangepwdComponent } from './home/changepwd/changepwd.component';









@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  AboutComponent,
  StudentloginComponent,
  AdminloginComponent,
  IndexComponent,
  ForgotpwdComponent,
  OtpComponent,
  ChangepwdComponent,
  
 



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    StudentModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AuthorizationService,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
