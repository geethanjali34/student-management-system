import { Component, OnInit } from '@angular/core';
import { DataService } from '../../admin/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {

  
  data:any[];
  user;
  constructor(private ds:DataService,private router:Router) { }

  ngOnInit() {

    this.user=this.ds.sendLoggedUser();

    this.ds.specificfee(this.user).subscribe(fee=>{
      if(fee['message']=="unauthorized access")
      {
        alert(fee['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
      this.data=fee['message']; 
      }
    })
  }

}
