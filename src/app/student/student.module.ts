import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { StudentComponent } from './student/student.component';
import { ProfileComponent } from './profile/profile.component';
import { MarksdetailsComponent } from './marksdetails/marksdetails.component';
import { FeestatusComponent } from './feestatus/feestatus.component';
import { AttendancestatusComponent } from './attendancestatus/attendancestatus.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { LogoutComponent } from './logout/logout.component';
import { FormsModule} from '@angular/forms';
import { LeavestatusComponent } from './leavestatus/leavestatus.component';



@NgModule({
  declarations: [StudentComponent, ProfileComponent, MarksdetailsComponent, FeestatusComponent, AttendancestatusComponent, NotificationsComponent, LogoutComponent, LeavestatusComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule
  ]
})
export class StudentModule { }
