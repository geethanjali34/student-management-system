import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentComponent } from './student/student.component';
import { ProfileComponent } from './profile/profile.component';
import { MarksdetailsComponent } from './marksdetails/marksdetails.component';
import { FeestatusComponent } from './feestatus/feestatus.component';
import { AttendancestatusComponent } from './attendancestatus/attendancestatus.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { LogoutComponent } from './logout/logout.component';
import { LeavestatusComponent } from './leavestatus/leavestatus.component';


const routes: Routes = [
  {
    path:'student',
    component:StudentComponent,
    children:[
      {
           path:'profile',
           component:ProfileComponent
     },
     {
      path:'marksdetails',
      component:MarksdetailsComponent
     },
    {
        path:'feestatus',
        component:FeestatusComponent
     },
     {
       path:'attendancestatus',
       component:AttendancestatusComponent
     },
     {
       path:'notifications',
       component:NotificationsComponent
     },
     {
       path:'leavestatus',
       component:LeavestatusComponent
     },
     {
       path:'logout',
       component:LogoutComponent
     }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
