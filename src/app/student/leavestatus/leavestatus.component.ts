import { Component, OnInit } from '@angular/core';
import { DataService } from '../../admin/data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-leavestatus',
  templateUrl: './leavestatus.component.html',
  styleUrls: ['./leavestatus.component.css']
})
export class LeavestatusComponent implements OnInit {


b:boolean=true;
user:any;
j:any;
accept:any;




dates:number[]=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

  constructor(private ds:DataService,private hc:HttpClient) { }

  ngOnInit() {
  
    this.user=this.ds.sendLoggedUser();
    this.ds.leaverequest(this.user).subscribe(request=>{
      this.accept=request['message']
    })
  
   
  }

  changeStatus(value)
  {
    if(value.no=="" || value.username=="" || value.leave=="" ||value.from=="" || value.to=="")
    {
          alert("Enter valid data")
    }
    else
    {

    console.log(value);
    this.j=this.ds.sendLoggedUser();
    value.username=this.j.username;
    
    this.hc.post('student/leave',value).subscribe(res=>{
      alert(res['message']);
  })
  this.b=false;
}
  }
}