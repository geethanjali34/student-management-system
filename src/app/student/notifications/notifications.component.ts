import { Component, OnInit } from '@angular/core';
import { DataService } from '../../admin/data.service';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

l:any[]=[];

  constructor(private ds:DataService) { }

  ngOnInit() {
      
    this.ds.sendToStudentNotify().subscribe(data=>{
      this.l=data['message'];
    })
    console.log(this.l);
  }

}
