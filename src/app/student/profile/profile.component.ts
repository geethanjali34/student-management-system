import { Component, OnInit } from '@angular/core';
import { DataService } from '../../admin/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  i:any[];
  data:any[];
user;
  constructor(private ds:DataService,private router:Router) { }

  ngOnInit() {

    this.user=this.ds.sendLoggedUser();
    this.ds.specificprofile(this.user).subscribe(profile=>{
      if(profile['message']=="unauthorized access")
      {
        alert(profile['message']); 
         this.router.navigate(['/home/login']);
      }
      else
      {
      this.data=profile['message'];  
      }
    })
  
  }

}
