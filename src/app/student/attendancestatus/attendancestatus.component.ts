import { Component, OnInit } from '@angular/core';
import { DataService } from '../../admin/data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-attendancestatus',
  templateUrl: './attendancestatus.component.html',
  styleUrls: ['./attendancestatus.component.css']
})
export class AttendancestatusComponent implements OnInit {

  d:any[];
  user;
  constructor(private ds:DataService,private router:Router) { }

  ngOnInit() {

    this.user=this.ds.sendLoggedUser();
    this.ds.specificattendance(this.user).subscribe(attendance=>{
      if(attendance['message']=="unauthorized access")
      {
        alert(attendance['message']);
        this.router.navigate(['/home/login']);
      }
      else
      {
        this.d=attendance['message']; 
      }
      
 });
  }


}
