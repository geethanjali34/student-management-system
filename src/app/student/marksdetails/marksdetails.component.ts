import { Component, OnInit } from '@angular/core';

import { DataService } from '../../admin/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marksdetails',
  templateUrl: './marksdetails.component.html',
  styleUrls: ['./marksdetails.component.css']
})
export class MarksdetailsComponent implements OnInit {


  y:any[];
  user;
  constructor(private ds:DataService,private router:Router) { }

  ngOnInit() {
    
   this.user=this.ds.sendLoggedUser();
    this.ds.specificmarks(this.user).subscribe(marks=>{
        if(marks['message']=="unauthorized access")
        {
          alert(marks['message']);
          this.router.navigate(['/home/login']);
        }
        else
        {
      this.y=marks['message'];
        }
    })
    
  }

}
