import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarksdetailsComponent } from './marksdetails.component';

describe('MarksdetailsComponent', () => {
  let component: MarksdetailsComponent;
  let fixture: ComponentFixture<MarksdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarksdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarksdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
