const jwt=require("jsonwebtoken");
const secret="secret";
var checkauthorization=(req,res,next)=>{
    console.log(req.headers);

    //read authorization in req object
    var token=req.headers['authorization'];
    //if token is found, check for validity
    if(token==undefined)
    {
        return res.json({"message":"unauthorized access"})
    }
    if(token.startsWith("Bearer "))
    {
        token=token.slice(7,token.length);
        jwt.verify(token,secret,(err,decoded)=>{
            if(err)
            {
                return res.json({message:"Session Expired....Please Relogin"});
                
            }
            //forward to next middleware or req handler
            next();
        })
    }
}
module.exports=checkauthorization;