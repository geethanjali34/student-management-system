const exp=require("express");
var studentroutes=exp.Router();

//import DBconfig.js file
const getdb=require('../DBconfig').getdb;
const initdb=require('../DBconfig').initdb;

//import bcrypt
var bcrypt=require('bcrypt');

//import jsonwebtoken
const jwt=require("jsonwebtoken");

//define secret key
const secretkey="secret";


//import middleware
const checkauthorization=require('../middlewares/checkauthorization');

//first call initdb function
initdb();

studentroutes.get("/profile",checkauthorization,(req,res)=>{
    console.log(req.params); 
    
    dbo=getdb();
    dbo.collection("profilecollection").find().toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
});


studentroutes.get("/feestatus",checkauthorization, (req,res)=>{
    
    dbo=getdb();
    dbo.collection("feecollection").find().toArray((err,success)=>{
        if(err) 
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
            console.log(success);
        }
    })
});


studentroutes.get("/attendancestatus",checkauthorization,(req,res)=>{
    
    dbo=getdb();
    dbo.collection("attendancecollection").find().toArray((err,success)=>{
        if(err) 
        {
            console.log('error in data reading');
            console.log(err);

        }
       
        else{
            res.json({"message":success});
            console.log(success);
        }
    })
});


studentroutes.get("/notifications",checkauthorization,(req,res)=>{
    
    dbo=getdb();
    dbo.collection("notificationcollection").find().toArray((err,success)=>{
        if(err) 
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
           
        }
    })
});


studentroutes.post('/leave',checkauthorization,(req,res,next)=>
{
  dbo=getdb()
  console.log(req.body)
  if(req.body=={})
  {
      res.json({message:"server did not receive data"})
  }else
  {
    dbo.collection("leavecollection").insertOne(req.body,(err,dataArray)=>{
        console.log(dataArray);
        if(err){
          next(err)
        }
        else{
          dbo.collection("requestcollection").deleteOne({username:{$eq:req.body.username}},(err,dataArray)=>{
            if(err)
            {
              next(err)
            }
            else
            {
               res.json({message:"Request sent successfully"})
            }
          })
           
        }
    })
  }
    
})

studentroutes.post("/studentlogin",(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();

            dbo.collection('profilecollection').find({username:{$eq:req.body.username}}).toArray((err,success)=>{
               console.log(success.length);
               
                if(success.length==0)
                {
                    res.json({message:"Invalid Username"});
                }
                else{

                    //comparing plain text password with hashed password
        bcrypt.compare(req.body.password,success[0].password,(err,result)=>{
                        console.log(result);
                        if(result==true)
                        {

                           //create and send json token
            const signedtoken=jwt.sign({username:success[0].username},secretkey,{expiresIn:60000})
                           console.log(signedtoken);
                           //send signed  token as response after succesful login

                           res.json({message:"success",token:signedtoken,data:success});
                          
                          
                        }
                        else{
                            res.json({message:"Invalid Password"});
                        }
                    })
                    
                }
            })
})




studentroutes.post("/specificstnmarks",checkauthorization,(req,res)=>{
    dbo=getdb();
    
    dbo.collection("markscollection").find({username:{$eq:req.body.username}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
})



studentroutes.get("/marksdetails",checkauthorization,(req,res)=>{
    
    dbo=getdb();
    dbo.collection("markscollection").find().toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
});



studentroutes.post("/specificstnfee",checkauthorization,(req,res)=>{
    dbo=getdb();
    
    dbo.collection("feecollection").find({username:{$eq:req.body.username}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
})

studentroutes.post("/specificstnprofile",checkauthorization,(req,res)=>{
    dbo=getdb();
    
    dbo.collection("profilecollection").find({username:{$eq:req.body.username}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
})


studentroutes.post("/specificstnattendance",checkauthorization,(req,res)=>{
    dbo=getdb();
    
    dbo.collection("attendancecollection").find({username:{$eq:req.body.username}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
})

studentroutes.post("/specificstnrequest",(req,res)=>{
    dbo=getdb();
    
    dbo.collection("requestcollection").find({username:{$eq:req.body.username}}).toArray((err,success)=>{
        if(err)
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
        }
    })
})

studentroutes.put("/update",(req,res)=>{
    res.json({"message":"student update working"})
})
studentroutes.delete("/remove",(req,res)=>{
    res.json({"message":"student delete working"})
});

module.exports=studentroutes;


