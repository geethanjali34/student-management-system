const exp=require("express");
var adminroutes=exp.Router();

//import DBconfig.js file
const getdb=require('../DBconfig').getdb;
const initdb=require('../DBconfig').initdb;

//first call initdb function
initdb();

var bcrypt=require('bcrypt');

//import jsonwebtoken
const jwt=require("jsonwebtoken");

//define secret key
const secretkey="secret";

//import body-parser
var bodyparser=require("body-parser");
adminroutes.use(bodyparser.json());


const accountSid = 'AC6905c00fd844a19c6f1ba1d4872a5985';
const authToken = '867b4865bff0d9a4fef9f7d339692712';
const client = require('twilio')(accountSid, authToken);


//import middleware
const checkauthorization=require('../middlewares/checkauthorization');

adminroutes.get("/leave",checkauthorization,(req,res)=>{
    console.log(req.body);
    dbo=getdb();
    dbo.collection("leavecollection").find().toArray((err,success)=>{
        if(err) 
        {
            console.log('error in data reading');
            console.log(err);

        }
        else if(success.length==0)

        {
          res.json({"message":"no data found"});
        }
        else{
            res.json({"message":success});
            console.log(success);
        }
    })
    
})

adminroutes.post("/adminlogin",(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();
     dbo.collection("admincollection").find({username:{$eq:req.body.username}}).toArray((err,success)=>{
        
        console.log(success.length);

        if(success.length==0)
        {
            res.json({message:"Invalid Username"});

        }
        else if(success[0].password!==req.body.password)
        {
            res.json({message:"Invalid Password"})
        }
        else{
            //create and send Json Token
            const signedtoken=jwt.sign({username:success[0].username},secretkey,{expiresIn:"60000"})
            console.log(signedtoken);

            //sent signed token as response after successful login
            res.json({message:"success",token:signedtoken});
        }
    })
})



adminroutes.post("/marks",checkauthorization,(req,res)=>{
    console.log(req.body);
    dbo=getdb();
    dbo.collection("markscollection").insert(req.body,(err,success)=>{
        if(err)
        {
            console.log('error in adding student marks');

        }
        else{
            res.json({message:"student marks added successfully"});
        }
    })
})
adminroutes.post("/fee",checkauthorization,(req,res)=>{
    console.log(req.body);
    dbo=getdb();
    dbo.collection("feecollection").insert(req.body,(err,success)=>{
        if(err)
        {
            console.log('error in adding student fee');

        }
        else{
            res.json({message:"student fee added successfully"});
        }
    })
})


adminroutes.post("/attendance",checkauthorization,(req,res)=>{
    console.log(req.body);
    dbo=getdb();
    dbo.collection("attendancecollection").insert(req.body,(err,success)=>{
        if(err)
        {
            console.log('error in adding student attendance');

        }
        else{
            res.json({message:"student attendance added successfully"});
           
        }
    })
})



adminroutes.post("/notification",checkauthorization,(req,res)=>{
    console.log(req.body);
    dbo=getdb();
    dbo.collection("notificationcollection").insert(req.body,(err,success)=>{
        if(err)
        {
            console.log('error in adding notification');

        }
        else{
            res.json({message:"notification sent successfully"});
           
        }
    })
})



adminroutes.post('/forgotpwd',(req,res,next)=>{
    console.log(req.body)
    dbo=getdb()
    dbo.collection('profilecollection').find({username:req.body.username}).toArray((err,userArray)=>{
        console.log(userArray);
        if(err){
            next(err)
        }
        else{
            if(userArray.length===0){
                res.json({message:"user not found"})
            }
            else{

                jwt.sign({username:userArray[0].username},secretkey,{expiresIn:3600},(err,token)=>{
                    if(err){
                     next(err);
                    }
                    else{
                        var OTP=Math.floor(Math.random()*99999)+11111;
                        console.log(OTP)
                        
                        client.messages.create({
                            body: OTP,
                            from: '+12053410386', // From a valid Twilio number
                            to: '+91 7989718161',  // Text this number
  
                        })
                        .then((message) => {
                            dbo.collection('otpcollection').insertOne({
                                OTP:OTP,
                                username:userArray[0].username,
                                OTPGeneratedTime:new Date().getTime()+15000
                        },(err,success)=>{
                            if(err){
                                next(err)
                            }
                            else{                                        
                                res.json({"message":"user found",
                                    "token":token,
                                    "OTP":OTP,
                                    "username":userArray[0].username
                                })
                            }
                        })
                        });

                    }
                    
                })
            }
        }
    })
})



adminroutes.post('/verifyotp',(req,res,next)=>{
    console.log(req.body)
    console.log(new Date().getTime())
    var currentTime=new Date().getTime()
    dbo.collection('otpcollection').find({"OTP":req.body.OTP}).toArray((err,OTPArray)=>{
        if(err){
            next(err)
        }
        else if(OTPArray.length===0){
            res.json({"message":"invalidOTP"})
        }
        else if(OTPArray[0].OTPGeneratedTime < req.body.currentTime){
            res.json({"message":"invalidOTP"})
        }
        else{
            
            dbo.collection('otpcollection').deleteOne({OTP:req.body.OTP},(err,success)=>{
                if(err){
                    next(err);
                }
                else{
                    console.log(OTPArray)
                    res.json({"message":"verifiedOTP"})
                }
            })
        }
    })
})



adminroutes.put('/changepwd',(req,res,next)=>{
    console.log(req.body)
    dbo=getdb()
    bcrypt.hash(req.body.password,6,(err,hashedPassword)=>{
        if (err) {
            next(err)
        } else {
            console.log(hashedPassword)
            dbo.collection('profilecollection').updateOne({username:req.body.username},{$set:{
                password:hashedPassword
            }},(err,success)=>{
                if(err){
                    next(err)
                }
                else{
                    res.json({"message":"password changed"})
                }
            }) 
        }
    })
    
  })




adminroutes.post('/addstudent',checkauthorization,(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();

    dbo.collection("profilecollection").find({name:{$eq:req.body.name}}).toArray((err,studentArray)=>{
      
      
      
        if(studentArray=="") 
       {       
    
    // hash the password req.body.password

      bcrypt.hash(req.body.password,6,(err,hashedpassword)=>{
        if(err)
        {
            next(err);
        }
        else{
            //replace plaintext password with hashedpassword.

            req.body.password=hashedpassword;
            dbo.collection('profilecollection').insert(req.body,(err,success)=>{
                if(err)
                {
                    console.log("error in add student");
                }
                else{
                    res.json({message:"student profile added successfully"});
                }
            })
        }
    })
}

else
{
res.json({"message":"Student already exist with that name"});

}
})
})

    

adminroutes.put("/modify",(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();
    //run update

    dbo.collection('profilecollection').update({username:{$eq:req.body.username}},{$set:{rollno:req.body.rollno,name:req.body.name,password:req.body.password,email:req.body.email,gender:req.body.gender,state:req.body.state,course:req.body.course,branch:req.body.branch}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            res.json({message:"success"});
            console.log(success);
        }
    })
})


adminroutes.put("/modifymarks",(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();
    //run update

    dbo.collection('markscollection').update({no:{$eq:req.body.no}},{$set:{name:req.body.name,username:req.body.username,course:req.body.course,branch:req.body.branch,total:req.body.total,gain:req.body.gain}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            res.json({message:"success"});
            console.log(success);
        }
    })
})


adminroutes.put("/modifyfee",(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();
    //run update

    dbo.collection('feecollection').update({no:{$eq:req.body.no}},{$set:{name:req.body.name,username:req.body.username,total:req.body.total,paid:req.body.paid,pending:req.body.pending}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            res.json({message:"success"});
            console.log(success);
        }
    })
})
adminroutes.put("/modifyattendance",(req,res,next)=>{
    console.log(req.body);
    dbo=getdb();
    //run update

    dbo.collection('attendancecollection').update({no:{$eq:req.body.no}},{$set:{name:req.body.name,username:req.body.username,course:req.body.course,branch:req.body.branch,monthly:req.body.monthly,weekly:req.body.weekly}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            res.json({message:"success"});
            console.log(success);
        }
    })
})


adminroutes.post('/saverequest',(req,res,next)=>
{
  dbo=getdb()
  console.log(req.body)
  if(req.body=={})
  {
      res.json({message:"server did not receive data"})
  }else
  {
    dbo.collection("requestcollection").insertOne(req.body,(err,success)=>{
        if(err){
          next(err)
        }


        else{
           
            dbo.collection("leavecollection").deleteOne({username:{$eq:req.body.username}},(err,dataArray)=>{
              if(err)
              {
                next(err)
              }
              else
              {
                 res.json({message:"Request Sent Successfully"})
                 res.json({data:dataArray})
              }
            })
        }
    })
  }
    
})


adminroutes.delete("/deletingprofile/:username",(req,res,next)=>{
   console.log(req.params);

   //delete document with rollno as req.params.rollno

   dbo=getdb();
   dbo.collection('profilecollection').deleteOne({username:{$eq:req.params.username}},(err,success)=>{
       if(err)
       {
           next(err);
       }
       else{
           // read remaining data and send to client

           dbo.collection("profilecollection").find().toArray((err,dataArray)=>{
               if(err)
               {
                   next(err);
               }
               else{
                   console.log(dataArray)
                   res.json({message:"Record Deleted", remdata:dataArray})
               }
           })
       }
   })

});

adminroutes.delete("/deletingmarks/:name",(req,res,next)=>{
    console.log(req.params);
 
    //delete document with rollno as req.params.rollno
 
    dbo=getdb();
    dbo.collection('markscollection').deleteOne({name:{$eq:req.params.name}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            // read remaining data and send to client
 
            dbo.collection("markscollection").find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err);
                }
                else{
                    console.log(dataArray);
                    res.json({message:"Record Deleted", remdata:dataArray})
                }
            })
        }
    })
 
 });


 adminroutes.delete("/deletingfee/:name",(req,res,next)=>{
    console.log(req.params);
 
    //delete document with rollno as req.params.rollno
 
    dbo=getdb();
    dbo.collection('feecollection').deleteOne({name:{$eq:req.params.name}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            // read remaining data and send to client
 
            dbo.collection("feecollection").find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err);
                }
                else{
                    console.log(dataArray);
                    res.json({message:"Record Deleted", remdata:dataArray})
                }
            })
        }
    })
 
 });


 adminroutes.delete("/deletingattendance/:name",(req,res,next)=>{
    console.log(req.params);
 
    //delete document with rollno as req.params.rollno
 
    dbo=getdb();
    dbo.collection('attendancecollection').deleteOne({name:{$eq:req.params.name}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else{
            // read remaining data and send to client
 
            dbo.collection("attendancecollection").find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err);
                }
                else{
                    console.log(dataArray);
                    res.json({message:"Record Deleted", remdata:dataArray})
                }
            })
        }
    })
 
 });


module.exports=adminroutes;


