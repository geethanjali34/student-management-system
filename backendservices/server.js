
//import express module
const exp=require("express");
const app=exp();

//import and use body-parser
const bodyparser=require("body-parser");
app.use(bodyparser.json());

//import path module
var path=require("path");

//coonecting angular app with server
app.use(exp.static(path.join(__dirname,'../dist/studentmanagement/')));

//import adminrouter
const adminroutes=require("./routes/adminroutes");
//import studentrouter
const studentroutes=require("./routes/studentroutes");
//use adminrouter
app.use("/admin",adminroutes);
//use studentrouter
app.use("/student",studentroutes);


app.use((err,req,res,next)=>{
    console.log(err);
});


//assign port number

app.listen(process.env.PORT || 8080,()=>{
    console.log('server running on 8080.. ');
}); 